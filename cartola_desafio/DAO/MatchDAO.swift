//
//  MatchDAO.swift
//  cartola_desafio
//
//  Created by Victor de Lima on 12/10/17.
//  Copyright © 2017 Victor de Lima. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class MatchDAO: NSObject {
	
	func clubs(completionHandler completion: @escaping(_ clubs:[Club?]?, _ cdError: CDError?)->Void) -> DataRequest?{
		
		if let url = self.matchURL() {
			let dataRequest = Alamofire.request(url).responseJSON(completionHandler: { (dataResponse) in
				
				if let values = dataResponse.result.value as? [String:Any]{
					
					// get clubs
					var clubs = [Club?]()
					if let clubsJSON = values["clubes"] as? [String:Any] {
						for (_,value) in clubsJSON {
							if let json = value as? [String:Any] {
								let club = Club(JSON: json)
								clubs.append(club)
							}
						}
					}
					
					
					completion(clubs, nil)
				} else {
					let cdError = CDError(withTitleError: "Erro", errorMessage: "Não foi possível carregar partidas", andCode: 10)
					completion(nil, cdError)
				}
				
			})
			return dataRequest
		} else {
			let cdError = CDError(withTitleError: "Erro", errorMessage: "Não foi possível carregar partidas", andCode: 0)
			completion(nil, cdError)
			return nil
		}
	}
	
	
	func match(completionHandler completion: @escaping(_ matches:[Match?]?, _ cdError:CDError?)->Void) -> DataRequest? {

		if let url = self.matchURL() {
			let dataRequest = Alamofire.request(url).responseJSON(completionHandler: { (dataResponse) in
			
				if let values = dataResponse.result.value as? [String:Any]{

					// get clubs
					var clubs = [Club?]()
					if let clubsJSON = values["clubes"] as? [String:Any] {
						for (_,value) in clubsJSON {
							if let json = value as? [String:Any] {
								let club = Club(JSON: json)
								clubs.append(club)
							}
						}
					}

					// get matches
					var matches = [Match?]()
					if let matchesArray = values["partidas"] as? [[String:Any]] {
						for json in matchesArray {
							let match = Match(JSON: json)
							matches.append(match)
						}
					}
					
					for match in matches {
						for club in clubs {
							if match?.clubHomeId == club?.id {
								match?.clubHome = club
							}
							
							if match?.clubVisitorId == club?.id {
								match?.clubVisitor = club
							}
						}
					}
					
					
					completion(matches, nil)
				} else {
					let cdError = CDError(withTitleError: "Erro", errorMessage: "Não foi possível carregar partidas", andCode: 10)
					completion(nil, cdError)
				}
				
			})
			return dataRequest
		} else {
			let cdError = CDError(withTitleError: "Erro", errorMessage: "Não foi possível carregar partidas", andCode: 0)
			completion(nil, cdError)
			return nil
		}
	}
	
}

// This code piece can be used like an extension from some kind of URLProvider.
// I just put here for convenience
extension MatchDAO {
	
	func matchURL() -> URL? {
		
		let url = URL(string: "https://api.cartolafc.globo.com/partidas")
		
		return url
	}
	
}
