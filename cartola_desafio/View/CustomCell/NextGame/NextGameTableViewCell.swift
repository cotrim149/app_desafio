//
//  NextGameTableViewCell.swift
//  cartola_desafio
//
//  Created by Victor de Lima on 12/10/17.
//  Copyright © 2017 Victor de Lima. All rights reserved.
//

import UIKit

class NextGameTableViewCell: UITableViewCell {

	@IBOutlet weak var homeShieldImage: UIImageView!
	@IBOutlet weak var homeAbbreviationLabel: UILabel!
	
	@IBOutlet weak var visitorShieldImage: UIImageView!
	@IBOutlet weak var visitorAbbreviationLabel: UILabel!
	
	@IBOutlet weak var matchDateLabel: UILabel!
	@IBOutlet weak var matchPlaceLabel: UILabel!
	
	static var identifier: String {
		return "nextGameTableViewCell"
	}
	
	static var nibName: String {
		return "NextGameTableViewCell"
	}
	
	static var cellHeight: CGFloat {
		return 195.0
	}

	var match: Match? {
		didSet {
			setupCell()
		}
	}
	
	override func awakeFromNib() {
        super.awakeFromNib()
    }

	func setupCell() {
		
		if let date = match?.matchDate {
			let dateFmt = DateFormatter()
			dateFmt.dateFormat = "dd/MM/yyyy HH:mm"
			let matchDate = dateFmt.string(from: date)
			self.matchDateLabel.text = matchDate
		}
		
		homeShieldImage.image = match?.clubHome?.shield
		homeAbbreviationLabel.text = match?.clubHome?.abbreviation
		
		visitorShieldImage.image = match?.clubVisitor?.shield
		visitorAbbreviationLabel.text = match?.clubVisitor?.abbreviation
		
		matchPlaceLabel.text = match?.matchPlace
	}
    
}
