//
//  TeamPositionTableViewCell.swift
//  cartola_desafio
//
//  Created by Victor de Lima on 10/10/17.
//  Copyright © 2017 Victor de Lima. All rights reserved.
//

import UIKit

class TeamPositionTableViewCell: UITableViewCell {

	@IBOutlet weak var shieldImage: UIImageView!
	@IBOutlet weak var teamNameLabel: UILabel!
	@IBOutlet weak var teamPositionLabel: UILabel!
	
	static var identifier: String {
		return "teamPositionTableViewCell"
	}

	static var nibName: String {
		return "TeamPositionTableViewCell"
	}
	
	static var cellHeight: CGFloat {
		return 90.0
	}
	
	var club: Club? {
		didSet {
			self.setupCell()
		}
	}
	
	override func awakeFromNib() {
        super.awakeFromNib()
    }

	fileprivate func setupCell() {
		shieldImage.image = club?.shield
		teamNameLabel.text = club?.name
		
		if let position = club?.position {
			teamPositionLabel.text = String(position)
		}
	}
    
}
