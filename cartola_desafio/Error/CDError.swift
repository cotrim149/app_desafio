//
//  CDError.swift
//  cartola_desafio
//
//  Created by Victor de Lima on 12/10/17.
//  Copyright © 2017 Victor de Lima. All rights reserved.
//

import UIKit

class CDError: NSObject {

	var codeError: Int?
	var titleMessage: String?
	var errorMessage: String?
	
	override init(){
		super.init()
	}

	init(withTitleError titleError:String?, errorMessage: String?, andCode codeError: Int?){
		self.titleMessage = titleError
		self.errorMessage = errorMessage
		self.codeError = codeError
	}

}
