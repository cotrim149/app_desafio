//
//  TransformUtil.swift
//  cartola_desafio
//
//  Created by Victor de Lima on 12/10/17.
//  Copyright © 2017 Victor de Lima. All rights reserved.
//

import UIKit
import ObjectMapper

class TransformUtil {

	static let dateTransformer: TransformOf<Date, String> = TransformOf<Date, String>(fromJSON: { (dateStr) -> Date? in
		return Date(string: dateStr, dateFormat: "yyyy-MM-dd HH:mm:ss")
	}, toJSON: { (date) -> String? in
		return date?.ISOString
	})

}
