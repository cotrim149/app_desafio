//
//  CustomActivityIndicator.swift
//  cartola_desafio
//
//  Created by Victor de Lima on 12/10/17.
//  Copyright © 2017 Victor de Lima. All rights reserved.
//

import UIKit

class CustomActivityIndicator : NSObject {
	
	static func lock(targetView view: UIView, _ duration: TimeInterval = 0.1) {
		if let _ = view.viewWithTag(10) {
			//View is already locked
		}
		else {
			let lockView = UIView(frame: view.bounds)
			lockView.backgroundColor = UIColor.black.withAlphaComponent(0.2)
			lockView.tag = 10
			
			let activity = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
			
			activity.translatesAutoresizingMaskIntoConstraints = false
			activity.alpha = 1
			
			lockView.addSubview(activity)
			
			activity.startAnimating()
			
			view.addSubview(lockView)
			view.bringSubview(toFront: lockView)
			
			let xCenterConstraint = NSLayoutConstraint(item: activity, attribute: .centerX, relatedBy: .equal, toItem: lockView, attribute: .centerX, multiplier: 1, constant: 0)
			
			let yCenterConstraint = NSLayoutConstraint(item: activity, attribute: .centerY, relatedBy: .equal, toItem: lockView, attribute: .centerY, multiplier: 1, constant: 0)
			
			
			NSLayoutConstraint.activate([xCenterConstraint, yCenterConstraint])
			
		}
	}
	
	static func unlock(targetView view: UIView, _ duration: TimeInterval = 0.1) {
		if let lockView = view.viewWithTag(10) {
			lockView.removeFromSuperview()
		}
	}
	
}
