//
//  TeamService.swift
//  cartola_desafio
//
//  Created by Victor de Lima on 11/10/17.
//  Copyright © 2017 Victor de Lima. All rights reserved.
//

import UIKit
import Alamofire

class TeamService: NSObject {

	static let matchDAO = MatchDAO()
	
	static func match(completionHandler completion: @escaping(_ matches: [Match?]?, _ cdError:CDError?)->Void) -> DataRequest? {
		
		return self.matchDAO.match(completionHandler: completion)
	}
	
	static func clubs(completionHandler completion: @escaping(_ clubs: [Club?]?, _ cdError: CDError?) ->Void) -> DataRequest? {
		return self.matchDAO.clubs(completionHandler: completion)
	}
	
}
