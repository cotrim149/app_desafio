//
//  Match.swift
//  cartola_desafio
//
//  Created by Victor de Lima on 12/10/17.
//  Copyright © 2017 Victor de Lima. All rights reserved.
//

import UIKit
import ObjectMapper

class Match: Mappable, Hashable {
	
	var hashValue: Int = 0

	static func ==(lhs: Match, rhs: Match) -> Bool {
		return (lhs.matchDate) == (rhs.matchDate)
	}

	var clubHomeId: Int?
	var clubVisitorId: Int?
	
	var matchDate: Date?
	var matchPlace: String?
	
	var clubHome:Club?
	var clubVisitor:Club?
	
	required init?(map: Map) {
	}
	
	func mapping(map: Map) {
		clubHomeId <- map["clube_casa_id"]
		clubVisitorId <- map["clube_visitante_id"]
		
		matchDate <- (map["partida_data"],TransformUtil.dateTransformer)
		matchPlace <- map["local"]
	}
}
