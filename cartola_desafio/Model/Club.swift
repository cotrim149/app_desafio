//
//  Club.swift
//  cartola_desafio
//
//  Created by Victor de Lima on 11/10/17.
//  Copyright © 2017 Victor de Lima. All rights reserved.
//

import UIKit
import ObjectMapper

class Club: Mappable, Hashable {
	
	var hashValue: Int = 0
	
	static func ==(lhs: Club, rhs: Club) -> Bool {
		return (lhs.position) == (rhs.position)
	}
	
	
	var id:Int?
	var name:String?
	var abbreviation: String?
	var position: Int?
	fileprivate var shieldsURL: [String : String]?
	var shield: UIImage?

	required init?(map: Map) {
	}

	func mapping(map: Map) {
		id <- map["id"]
		name <- map["nome"]
		abbreviation <- map["abreviacao"]
		position <- map["posicao"]
		shieldsURL <- map["escudos"]
		
		shield = self.shieldImage(ofURL: shieldsURL?["60x60"])
	}
	
	private func shieldImage(ofURL stringURL:String?) -> UIImage? {
		guard let urlPath = stringURL,
			let url = URL(string: urlPath) else {
			return nil
		}
		
		var data: Data?

		do {
			data = try Data(contentsOf: url)
		}catch {
			return nil
		}

		if let imageData = data {
			return UIImage(data:imageData)
		} else {
			return nil
		}
	}
}
