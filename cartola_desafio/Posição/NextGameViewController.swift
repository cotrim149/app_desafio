//
//  NextGameViewController.swift
//  cartola_desafio
//
//  Created by Victor de Lima on 13/10/17.
//  Copyright © 2017 Victor de Lima. All rights reserved.
//

import UIKit
import Alamofire

class NextGameViewController: UIViewController {

	@IBOutlet weak var tableView: UITableView!
	fileprivate var matches: [Match?]?
	private var dataRequest: DataRequest?
	fileprivate var isMatchesSortedAscending = true

	override func viewDidLoad() {
        super.viewDidLoad()
		registerCells()
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)

		if matches == nil {
			requestMatch()
		}
		
		addSortButton()
		
		self.navigationController?.navigationBar.topItem?.title = "Próximas rodadas"
		
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		dataRequest?.cancel()
	}
	
	func registerCells() {
		tableView.register(UINib(nibName: NextGameTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: NextGameTableViewCell.identifier)
	}
	
	func requestMatch() {
		CustomActivityIndicator.lock(targetView: self.view)
		
		self.dataRequest = TeamService.match { (matches, cdError) in
			
			if let error = cdError {
				let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
				UIAlertController.alert(withTitle: error.titleMessage, alertMessage: error.errorMessage, actions: [okAction], onController: self)
			} else {
				self.matches = matches
				self.tableView.reloadData()
				self.sortMatches()
			}
			
			CustomActivityIndicator.unlock(targetView: self.view)

		}
	}
	
	func addSortButton() {
		let rightBarButton = UIBarButtonItem(image: #imageLiteral(resourceName: "sort"), style: .plain , target: self, action: #selector(sortMatches))
		
		self.navigationController?.navigationBar.topItem?.rightBarButtonItem = rightBarButton
	}

	@objc func sortMatches() {
		
		self.matches?.sort(by: { (matchA, matchB) -> Bool in
			
			guard let matchDateA = matchA?.matchDate , let matchDateB = matchB?.matchDate else {
				return false
			}
			
			if self.isMatchesSortedAscending {
				return matchDateA < matchDateB
			} else {
				return matchDateA > matchDateB
			}
			
		})
		
		DispatchQueue.main.async {
			self.isMatchesSortedAscending = !self.isMatchesSortedAscending
			self.tableView.reloadData()
		}
	}
	
}

extension NextGameViewController: UITableViewDelegate, UITableViewDataSource {
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		
		if let matches = self.matches {
			return matches.count
		} else {
			return 0
		}
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		if let _ = self.matches {
			return NextGameTableViewCell.cellHeight
		} else {
			return 0
		}
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

		if let _ = matches {
			return generateNexGameCell(withIndexPath: indexPath)
		} else {
			return UITableViewCell()
		}
	}
}

extension NextGameViewController {
	func generateNexGameCell(withIndexPath indexPath:IndexPath) -> NextGameTableViewCell {
		
		let cell = tableView.dequeueReusableCell(withIdentifier: NextGameTableViewCell.identifier, for: indexPath) as! NextGameTableViewCell
		
		cell.match = matches?[indexPath.row]
		cell.selectionStyle = .none
		return cell
	}
}
