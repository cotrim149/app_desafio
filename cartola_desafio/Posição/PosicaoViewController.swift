//
//  PosicaoViewController.swift
//  cartola_desafio
//
//  Created by Victor de Lima on 10/10/17.
//  Copyright © 2017 Victor de Lima. All rights reserved.
//

import UIKit
import Alamofire

class PosicaoViewController: UIViewController {

	@IBOutlet weak var tableView: UITableView!
	
	fileprivate var clubs : [Club?]?
	private var dataRequest : DataRequest?
	fileprivate var isClubsSortedAscending = true
	
	override func viewDidLoad() {
        super.viewDidLoad()
		self.registerCells()
    }

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)

		self.navigationController?.navigationBar.topItem?.title = "Posição"

		if clubs == nil {
			requestMatches()
		}
		
		addSortButton()

	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		dataRequest?.cancel()
	}
	
	private func registerCells() {
		tableView.register(UINib(nibName: TeamPositionTableViewCell.nibName, bundle: Bundle.main), forCellReuseIdentifier: TeamPositionTableViewCell.identifier)
//		tableView.registerNibFrom(TeamPositionTableViewCell.self)
	}
	
	func requestMatches() {
		
		CustomActivityIndicator.lock(targetView: self.view)
		
		self.dataRequest = TeamService.clubs { (clubs, cdError) in
			
			if let error = cdError {
				let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
				
				UIAlertController.alert(withTitle: error.titleMessage, alertMessage: error.errorMessage, actions: [okAction], onController: self)
			} else {
				self.clubs = clubs
				self.sortClubs()
				self.tableView.reloadData()
			}
			
			CustomActivityIndicator.unlock(targetView: self.view)
		}
	}
	
	func addSortButton() {
		let rightBarButton = UIBarButtonItem(image: #imageLiteral(resourceName: "sort"), style: .plain , target: self, action: #selector(sortClubs))

		self.navigationController?.navigationBar.topItem?.rightBarButtonItem = rightBarButton
	}
	
	@objc func sortClubs() {
		
		self.clubs?.sort(by: { (clubA, clubB) -> Bool in
			
			guard let positionA = clubA?.position, let positionB = clubB?.position else {
				return false
			}
			
			if self.isClubsSortedAscending {
				return positionA < positionB
			} else {
				return positionA > positionB
			}
			
		})
		
		DispatchQueue.main.async {
			self.isClubsSortedAscending = !self.isClubsSortedAscending
			self.tableView.reloadData()
		}
		
	}
	
}

extension PosicaoViewController: UITableViewDelegate, UITableViewDataSource {
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		
		guard let _ = self.clubs else {
			return 0
		}

		return 10
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return TeamPositionTableViewCell.cellHeight
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

		guard let _ = self.clubs else {
			return UITableViewCell()
		}
		
		return generateClubCell(forIndexPath: indexPath)
	}
}

extension PosicaoViewController {
	func generateClubCell(forIndexPath indexPath:IndexPath) -> TeamPositionTableViewCell{
		
		let cell = tableView.dequeueReusableCell(withIdentifier: TeamPositionTableViewCell.identifier, for: indexPath) as! TeamPositionTableViewCell
		cell.club = clubs?[indexPath.row]
		cell.selectionStyle = .none
		return cell
	}
}
