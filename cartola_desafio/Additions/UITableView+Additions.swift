//
//  UITableView+Additions.swift
//  cartola_desafio
//
//  Created by Victor de Lima on 11/10/17.
//  Copyright © 2017 Victor de Lima. All rights reserved.
//

import UIKit

extension UITableView {
	
	func registerNibFrom(_ cellClass: UITableViewCell.Type) {
		
		let nibName = cellClass.value(forKey: "nibName") as! String
		let cellIdentifier = cellClass.value(forKey: "identifier") as! String
		
		let nib = UINib(nibName: nibName, bundle: nil)
		self.register(nib, forCellReuseIdentifier: cellIdentifier)
	}
}

