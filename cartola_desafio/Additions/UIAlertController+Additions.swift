//
//  UIAlertController+Additions.swift
//  cartola_desafio
//
//  Created by Victor de Lima on 12/10/17.
//  Copyright © 2017 Victor de Lima. All rights reserved.
//

import UIKit

extension UIAlertController {

	static func alert(withTitle alertTitle:String?, alertMessage:String?, actions: [UIAlertAction], onController controller: UIViewController) {

		guard let title = alertTitle, let message = alertMessage else {
			assertionFailure("Title and Message of alert must be implemented!")
			return
		}
		
		let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
		
		for action in actions {
			alert.addAction(action)
		}
		
		controller.present(alert, animated: true, completion: nil)
		
	}
	
}
