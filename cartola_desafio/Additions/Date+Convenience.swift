//
//  Date+Convenience.swift
//  cartola_desafio
//
//  Created by Victor de Lima on 12/10/17.
//  Copyright © 2017 Victor de Lima. All rights reserved.
//

import UIKit

extension Date {
	init?(string: String?, dateFormat: String = "yyyy-MM-dd'T'HH:mm:ssZ") {
		guard let dateString = string else {
			return nil
		}
		
		let dtFmt = DateFormatter()
		dtFmt.dateFormat = dateFormat
		
		if let date = dtFmt.date(from: dateString) {
			self.init(timeInterval: 0, since: date)
		} else {
			return nil
		}
	}
	
	var ISOString: String {
		get {
			let dtFmt = DateFormatter()
			
			dtFmt.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
			dtFmt.timeZone = TimeZone.current
			
			return dtFmt.string(from: self)
		}
	}
}
